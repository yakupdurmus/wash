/**
 * @format
 */

import {AppRegistry} from 'react-native';
import Container from './src';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Container);
